package org.apache.cordova.camera;

public class AlbumInfo {
	public String path = "";
	public String name = "";
	public String image = "";

	public AlbumInfo(String path, String name, String image) {
		this.path = path;
		this.name = name;
		this.image = image;
	}

}
