package org.apache.cordova.camera;

import java.io.File;
import java.io.IOException;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.AbsListView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.nostra13.universalimageloader.cache.disc.naming.Md5FileNameGenerator;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.assist.SimpleImageLoadingListener;
import com.nostra13.universalimageloader.core.assist.*;

public class MultiPhotoSelectActivity extends BaseActivity {

	private ArrayList<String> imageUrls;
	private ArrayList<String> imageUrlsID;
	private DisplayImageOptions options;
	private ImageAdapter imageAdapter;
	private int maxSelectedLimit = 0;
	private GridView gv = null;
	private String album = "";
	
	private String LOG_TAG = "PluginTag";

	public void reInitImageLoader() {
		
		    if(ImageLoader.getInstance().isInited()) {
				ImageLoader.getInstance().stop();
				ImageLoader.getInstance().clearDiscCache();
				ImageLoader.getInstance().clearMemoryCache();
				ImageLoader.getInstance().destroy();
		    }
		
	        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(getApplicationContext())
	                .threadPoolSize(3)
	                .threadPriority(Thread.NORM_PRIORITY - 2)
	                .memoryCacheSize(3000000) // 1.5 Mb
	                .denyCacheImageMultipleSizesInMemory()
	                .discCacheFileNameGenerator(new Md5FileNameGenerator()) // Not necessary in common
	                .build();
	
	        // Initialize ImageLoader with configuration.
	        ImageLoader.getInstance().init(config);
    }
	
	
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Log.d("PhotoPlugin", "Start create" );
	

		try {

					RelativeLayout rl = new RelativeLayout(this.getBaseContext());
					rl.setLayoutParams(new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.FILL_PARENT));
					
					gv = new GridView(this.getBaseContext());
					LayoutParams gridLP = new LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.FILL_PARENT);
					gv.setGravity(Gravity.CENTER_HORIZONTAL);
					gv.setGravity(Gravity.CENTER);
					gv.setNumColumns(3);
					gv.setVerticalSpacing(1);
					gv.setHorizontalSpacing(1);
					gv.setLayoutParams(gridLP);
					
					Button select = new Button(this.getBaseContext());
					select.setMinWidth(200);
					select.setText("Select");
					RelativeLayout.LayoutParams selectLP = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
					selectLP.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
					selectLP.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
					selectLP.addRule(RelativeLayout.CENTER_HORIZONTAL);
					select.setLayoutParams(selectLP);
					
					Button cancel = new Button(this.getBaseContext());
					cancel.setMinWidth(200);
					cancel.setText("Cancel");
					RelativeLayout.LayoutParams cancelLP = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
					cancelLP.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
					cancelLP.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
					cancel.setLayoutParams(cancelLP);
					
					
					
					rl.addView(gv);
					rl.addView(cancel);
					rl.addView(select);

					setContentView(rl.getRootView());
			
			
			//setContentView(R.layout.ac_image_grid);

			Intent intent = getIntent();
			this.maxSelectedLimit = intent.getIntExtra("maxSelectedLimit", -1);
			this.album = intent.getStringExtra("album");
			
			final String orderBy = MediaStore.Images.Thumbnails.IMAGE_ID + " DESC";
			final String orderBy2 = MediaStore.Images.Media._ID + " DESC";
			
			this.imageUrls = new ArrayList<String>();
			this.imageUrlsID = new ArrayList<String>();
			
			String[] projection = new String[] {MediaStore.Images.Thumbnails._ID, MediaStore.Images.Thumbnails.IMAGE_ID, MediaStore.Images.Thumbnails.DATA};
			String[] filePathColumn = {MediaStore.Images.Media._ID, MediaStore.Images.Media.DATA };
			
			Cursor thumbnails = getContentResolver().query(MediaStore.Images.Thumbnails.EXTERNAL_CONTENT_URI, projection, null, null, orderBy);
			Cursor images = getContentResolver().query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, filePathColumn, null,null, orderBy2);
			//Log.d(LOG_TAG,"thumb count: " + thumbnails.getCount() +"");
			//Log.d(LOG_TAG,"images count: " +  images.getCount() +"");
			
			
			String selectedAlbumPath = this.album;
			
			AlbumActivity.scanAlbumsInfo(this.getBaseContext());
			TreeMap<Integer, String> albumPictureList = (TreeMap<Integer, String>)AlbumActivity.getAlbumPictureInfo(getBaseContext(), AlbumActivity.ALBUM_LIST_PICTURE);
			if(albumPictureList == null) {
				selectedAlbumPath = "/";
			}

			if( this.album.equals("") ) {
				selectedAlbumPath = "/";
			}
			Log.d("PhotoPlugin", "Album" );
			
			//Log.d(LOG_TAG, "selected album path "+selectedAlbumPath );
			//Log.d(LOG_TAG, "picture list count"+albumPictureList.size() );
			Log.d("PhotoPlugin", "selected album path "+selectedAlbumPath );
			Log.d("PhotoPlugin", "picture list count"+images.getCount() );
			Log.d("PhotoPlugin", "picture list count"+thumbnails.getCount() );
			Log.d("PhotoPlugin", "picture list count"+albumPictureList.size() );
			if ((images.getCount() - thumbnails.getCount() > 10) || thumbnails.getCount() == 0 ) {
				int indexDATA = images.getColumnIndex(MediaStore.Images.Media.DATA);
				int indexIMAGE_ID = images.getColumnIndex(MediaStore.Images.Media._ID);
				
				for (int i = 0; i < images.getCount(); i++) {
					images.moveToPosition(i);
				
					String path = albumPictureList.get(Integer.parseInt(images.getString(indexIMAGE_ID)));
					//Log.d(LOG_TAG, "First compare: "+path );
					//Log.d(LOG_TAG, "Second compar: "+selectedAlbumPath );
					//Log.d(LOG_TAG, "................................." );
					if(path != null && path.contains(selectedAlbumPath)) {
						imageUrls.add(images.getString(indexDATA));
						imageUrlsID.add(images.getString(indexIMAGE_ID));
					}
				}
				
				//Log.d(LOG_TAG, "Finished scan"+imageUrls.size() );
			} else {
				int indexDATA = thumbnails.getColumnIndex(MediaStore.Images.Thumbnails.DATA);
				int indexIMAGE_ID = thumbnails.getColumnIndex(MediaStore.Images.Thumbnails.IMAGE_ID);
				
				for (int i = 0; i < thumbnails.getCount(); i++) {
					thumbnails.moveToPosition(i);
				
					String path = albumPictureList.get(Integer.parseInt(thumbnails.getString(indexIMAGE_ID)));
					//Log.d(LOG_TAG, "First compare: "+path );
					//Log.d(LOG_TAG, "Second compar: "+selectedAlbumPath );
					//Log.d(LOG_TAG, "................................." );
					if(path != null && path.contains(selectedAlbumPath)) {
						imageUrls.add(thumbnails.getString(indexDATA));
						imageUrlsID.add(thumbnails.getString(indexIMAGE_ID));
					}
				}
				
				//Log.d(LOG_TAG, "Finished scan"+imageUrls.size() );
			}
			
			Log.d("PhotoPlugin", "After scan"+imageUrls.size() );
			
			Bitmap bitmap = Bitmap.createBitmap(100, 100, Bitmap.Config.RGB_565);
			bitmap.eraseColor(Color.BLACK);
			BitmapDrawable d = new BitmapDrawable(getResources(), bitmap); 
			//Log.d("debug", "Create bitmap");
			options = new DisplayImageOptions.Builder()
				.showImageOnLoading(d) // resource or drawable
				.showImageForEmptyUri(d) // resource or drawable
				.showImageOnFail(d)
				.cacheInMemory(true)
				.cacheOnDisc(true)
				.imageScaleType(ImageScaleType.IN_SAMPLE_POWER_OF_2)
				.build();

			imageAdapter = new ImageAdapter(this, imageUrls);
			//imageAdapter.notifyDataSetChanged();
		    //Log.d("PhotoPlugin", "image adapter" );
			
			/*GridView gridView = (GridView) findViewById(R.id.gridview);
			gridView.setAdapter(imageAdapter);*/
			gv.setAdapter(imageAdapter);
			//gv.refreshDrawableState();
			//if(ImageLoader.getInstance().isInited() == false ) {
			reInitImageLoader();
			//}
            Log.d("PhotoPlugin", "Reinit scan" );
			
			select.setOnClickListener(new OnClickListener() {
				
				public void onClick(View v) {
					try {

						ArrayList<String> selectedItems = imageAdapter.getCheckedItems();
						ArrayList<Integer> selectedItemsID = imageAdapter.getCheckedItemsID();
						/*Toast.makeText(MultiPhotoSelectActivity.this, "Total photos selected: "+selectedItems.size(), Toast.LENGTH_SHORT).show();
						Log.d("debug", "Selected Items: " + selectedItems.toString());*/
						ArrayList<Uri> uris = new ArrayList<Uri>();
						
						final int len = selectedItems.size();
						int cnt = 0;

						for (int i=0; i<len; i++)
		            	{
		            		if (selectedItems.get(i) != null){

		            			cnt++;
		            			String[] filePathColumn = { MediaStore.Images.Media.DATA };
		            			
		            			String selectedID =  imageUrlsID.get(selectedItemsID.get(i));

		            		    Cursor images = getContentResolver().query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, filePathColumn, MediaStore.Images.Media._ID + "=?", new String[] {selectedID}, null);

		            		    String filePath = selectedItems.get(i);
		            		    if (images != null && images.moveToFirst()) {
		            		        filePath = images.getString(images.getColumnIndex(filePathColumn[0]));
		            		    }

		            		    File fileIn = new File(filePath);
		            		    Uri u = Uri.fromFile(fileIn);
		            		    uris.add(u);

		            		}
		            	}
						
						Intent data = new Intent();
						data.putParcelableArrayListExtra("selectedImagesArray", uris);
						setResult(RESULT_OK,data);
						System.gc();
						ImageLoader.getInstance().stop();
						finish();
					}catch(Exception e) {
						
					}
				}
			});

			cancel.setOnClickListener(new OnClickListener() {
				
				public void onClick(View v) {
					try {
						//disableAll();
						Intent data = new Intent();
						setResult(RESULT_CANCELED,data);
						//imagecursor.close();
						//imagecursorPost.close();
						ImageLoader.getInstance().stop();
						finish();
					}catch(Exception e) {
						
					}
				}
			});
		} catch(Exception e) {
			Log.d("PhotoPlugin", e.getMessage() );
			finish();
		}
	}

	@Override
	protected void onStop() {
		ImageLoader.getInstance().stop();
		super.onStop();
	}

	public void btnChoosePhotosClick(View v){
		
		ArrayList<String> selectedItems = imageAdapter.getCheckedItems();
		Toast.makeText(MultiPhotoSelectActivity.this, "Total photos selected: "+selectedItems.size(), Toast.LENGTH_SHORT).show();
		//Log.d(MultiPhotoSelectActivity.class.getSimpleName(), "Selected Items: " + selectedItems.toString());
	}

	public class ImageAdapter extends BaseAdapter {
		
		ArrayList<String> mList;
		LayoutInflater mInflater;
		Context mContext;
		SparseBooleanArray mSparseBooleanArray;
		

		public ImageAdapter(Context context, ArrayList<String> imageList) {
			// TODO Auto-generated constructor stub
			mContext = context;
			mInflater = LayoutInflater.from(mContext);
			mSparseBooleanArray = new SparseBooleanArray();
			mList = new ArrayList<String>();
			this.mList = imageList;

		}
		
		public ArrayList<String> getCheckedItems() {
			ArrayList<String> mTempArry = new ArrayList<String>();

			for(int i=0;i<mList.size();i++) {
				if(mSparseBooleanArray.get(i)) {
					mTempArry.add(mList.get(i));
				}
			}

			return mTempArry;
		}
		
		public ArrayList<Integer> getCheckedItemsID() {
			ArrayList<Integer> mTempArry = new ArrayList<Integer>();
			
			for(int i=0;i<mList.size();i++) {
				if(mSparseBooleanArray.get(i)) {
					mTempArry.add(i);
				}
			}
			
			return mTempArry;
		}
		
		@Override
		public int getCount() {
			return imageUrls.size();
		}

		@Override
		public Object getItem(int position) {
			return null;
		}

		@Override
		public long getItemId(int position) {
			return position;
		}
		
		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			ViewHolder holder;
			if(convertView == null) {
				Log.d("PhotoPlugin", "convertView is null");
				holder = new ViewHolder();

				DisplayMetrics dm = new DisplayMetrics();
			    getWindowManager().getDefaultDisplay().getMetrics(dm);
			    int lenght = dm.widthPixels/3;

				RelativeLayout rl = new RelativeLayout(getBaseContext());
				AbsListView.LayoutParams rlLP = new AbsListView.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.FILL_PARENT);
				rl.setLayoutParams(rlLP);
				
				ImageView iv = new ImageView(MultiPhotoSelectActivity.this);
				RelativeLayout.LayoutParams ivLP = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
				ivLP.height = lenght;
				ivLP.addRule(RelativeLayout.CENTER_IN_PARENT);
				iv.setScaleType(ScaleType.CENTER_CROP);
				iv.setAdjustViewBounds(false);
				iv.setClickable(true);
				iv.setLayoutParams(ivLP);
				
				CheckBox cb = new CheckBox(getBaseContext());
				RelativeLayout.LayoutParams cbLP = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
				cbLP.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
				cbLP.addRule(RelativeLayout.ALIGN_PARENT_TOP);
				cb.setLayoutParams(cbLP);
				cb.setGravity(Gravity.TOP);
				cb.setBackgroundColor(Color.GRAY);
				
				rl.addView(iv);
				rl.addView(cb);

				convertView = rl.getRootView();
				holder.imageview = iv;
				holder.checkbox = cb;

				convertView.setTag(holder);
			}
			else {
				holder = (ViewHolder) convertView.getTag();
				Log.d("PhotoPlugin", "convertView isn't null");
			}
			convertView.setId(position);
			holder.checkbox.setId(position);
			holder.imageview.setId(position);
			holder.imageview.setTag(holder.checkbox);

			CheckBox mCheckBox = holder.checkbox;
			ImageView imageView = holder.imageview;

			Log.d("PhotoPlugin", "file://"+imageUrls.get(position));
			
			
			
			//imageLoader.resume();
			
			mCheckBox.setTag(position);
			mCheckBox.setChecked(mSparseBooleanArray.get(position));
			mCheckBox.setOnCheckedChangeListener(mCheckedChangeListener);
			imageView.setOnClickListener(new View.OnClickListener() {
				
				public void onClick(View v) {
					ImageView iv = (ImageView) v;
					CheckBox cb = (CheckBox) iv.getTag();
					
					ArrayList<String> selectedItems = imageAdapter.getCheckedItems();
					int id = iv.getId();
					int cnt = imageAdapter.getCheckedItems().size();
					if(maxSelectedLimit != -1 & cnt >= maxSelectedLimit ) {
						Toast.makeText(getApplicationContext(),
								"You have exceeded the limit of the photo selection. Your limit is " + cnt,
								Toast.LENGTH_LONG).show();
						cb.setChecked(false);
					} else {
						cb.setChecked(!cb.isChecked());
						mSparseBooleanArray.put(id, cb.isChecked());
					}
					
				}
			});
			
			ImageLoader.getInstance().displayImage("file://"+imageUrls.get(position), imageView, options, new SimpleImageLoadingListener() {

			@Override
		    public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
				int orientation = 0;
				Matrix matrix = new Matrix();

				int index = 0;
				/*for (int i=0; i < imageUrls.size(); i++ ) {
					if(imageUri.equalsIgnoreCase("file://"+imageUrls.get(i))) {
						index = i;
					}
				}*/
				Log.d("PhotoPlugin", "Loading complete2");
				index = view.getId();
				
				String[] filePathColumn = { MediaStore.Images.Media.ORIENTATION };
    			String selectedID =  imageUrlsID.get(index);
    			
    		    Cursor images = getContentResolver().query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, filePathColumn, MediaStore.Images.Media._ID + "=?", new String[] {selectedID}, null);
    		    if (images != null && images.moveToFirst()) {
    		    	orientation = images.getInt(0);
    		    	Log.d("PhotoPlugin", "iamge not null");
    		    } else {
    		    	Log.d("PhotoPlugin", "iamge is null");
    		    }
    		    

				if (orientation != 0) {
					if(orientation == 180) {
						matrix.postRotate(180);
					} else if(orientation == 90) {
						matrix.postRotate(90);
					} else if (orientation == -90) {
						matrix.postRotate(-90);
					}
					loadedImage = Bitmap.createBitmap(loadedImage, 0, 0, loadedImage.getWidth(), loadedImage.getHeight(), matrix, true);
					ImageView image = (ImageView)view;
					Log.d("PhotoPlugin", "after recreate image");
					image.setImageBitmap(loadedImage);
				} else {
					Log.d("PhotoPlugin", "not recreate image");
					ImageView image = (ImageView)view;
					image.setImageBitmap(loadedImage);
				}
				images.close();
		    }
		});
			
			return convertView;
		}
		
		OnCheckedChangeListener mCheckedChangeListener = new OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				ArrayList<String> selectedItems = imageAdapter.getCheckedItems();
				int id = (Integer) buttonView.getTag();
				int cnt = imageAdapter.getCheckedItems().size();
				if(maxSelectedLimit != -1 & cnt >= maxSelectedLimit ) {
					Toast.makeText(getApplicationContext(),
							"You have exceeded the limit of the photo selection. Your limit is " + cnt,
							Toast.LENGTH_LONG).show();
					buttonView.setChecked(false);
				} else {
					mSparseBooleanArray.put(id, isChecked);
				}
			}
		};
	}
	class ViewHolder {
		ImageView imageview;
		CheckBox checkbox;
		int id;
	}
	
	
}